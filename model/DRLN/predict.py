import torch
import sys
sys.path.append('model/DRLN/code')

import os
from model.DRLN.code.option import args
from model.DRLN.code import utility
from model_tester import ModelTesterBase
from model.DRLN.code.model.drln import DRLN


class DRLN_ModelTester(ModelTesterBase):
    def __init__(self, scale):
        self.model_name = 'DRLN'
        self.scale = scale
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        args.pre_train = "train-jobs/DRLN/DRLN_BIX{}.pt".format(self.scale)

        self.model = DRLN(args).to(self.device)

        self.load(
            args.pre_train,
            pre_train=args.pre_train,
            resume=args.resume,
            cpu=args.cpu
        )

    def get_model(self):
        if args.n_GPUs == 1:
            return self.model
        else:
            return self.model.module

    def load(self, apath, pre_train='.', resume=-1, cpu=False):
        if cpu:
            kwargs = {'map_location': lambda storage, loc: storage}
        else:
            kwargs = {}

        if resume == -1:
            self.get_model().load_state_dict(
                torch.load(
                    os.path.join(apath, 'model', 'model_latest.pt'),
                    **kwargs
                ),
                strict=False
            )
        elif resume == 0:
            if pre_train != '.':
                print('Loading model from {}'.format(pre_train))
                self.get_model().load_state_dict(
                    torch.load(pre_train, **kwargs),
                    strict=False
                )
        else:
            self.get_model().load_state_dict(
                torch.load(
                    os.path.join(apath, 'model', 'model_{}.pt'.format(resume)),
                    **kwargs
                ),
                strict=False
            )

    def forward_chop(self, x, shave=10, min_size=160000):
        scale = self.scale
        n_GPUs = min(4, 4)
        b, c, h, w = x.size()
        h_half, w_half = h // 2, w // 2
        h_size, w_size = h_half + shave, w_half + shave
        lr_list = [
            x[:, :, 0:h_size, 0:w_size],
            x[:, :, 0:h_size, (w - w_size):w],
            x[:, :, (h - h_size):h, 0:w_size],
            x[:, :, (h - h_size):h, (w - w_size):w]]

        if w_size * h_size < min_size:
            sr_list = []
            for i in range(0, 4, n_GPUs):
                lr_batch = torch.cat(lr_list[i:(i + n_GPUs)], dim=0)
                sr_batch = self.model(lr_batch)
                sr_list.extend(sr_batch.chunk(n_GPUs, dim=0))
        else:
            sr_list = [
                self.forward_chop(patch, shave=shave, min_size=min_size) \
                for patch in lr_list
            ]

        h, w = scale * h, scale * w
        h_half, w_half = scale * h_half, scale * w_half
        h_size, w_size = scale * h_size, scale * w_size
        shave *= scale

        output = x.new(b, c, h, w)
        output[:, :, 0:h_half, 0:w_half] \
            = sr_list[0][:, :, 0:h_half, 0:w_half]
        output[:, :, 0:h_half, w_half:w] \
            = sr_list[1][:, :, 0:h_half, (w_size - w + w_half):w_size]
        output[:, :, h_half:h, 0:w_half] \
            = sr_list[2][:, :, (h_size - h + h_half):h_size, 0:w_half]
        output[:, :, h_half:h, w_half:w] \
            = sr_list[3][:, :, (h_size - h + h_half):h_size, (w_size - w + w_half):w_size]

        return output

    def set_manual_seed(self):
        self.set_manual_seed_pt()

    def prepare_input(self, img_np):
        return self.prepare_input_pt(img_np)

    def do_inference(self, img_tensor):
        with torch.no_grad():
            sr = self.forward_chop(img_tensor)
            sr = utility.quantize(sr, 255)
            # sr -= sr.min()
            # sr /= sr.max()
        return self.convert_tensor_to_np_pt(sr)
