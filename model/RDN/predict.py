import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import sys
sys.path.append('../../code')
from model.RDN.model import RDN
import tensorflow as tf
import model.RDN.configs as configs
from model_tester import ModelTesterBase

class RDN_ModelTester(ModelTesterBase):
    def __init__(self, scale):
        self.model_name = 'RDN'
        self.scale = scale

        self.config = tf.ConfigProto()
        self.config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=self.config)

        rdn = RDN(self.sess,
                  c_dim=configs.c_dim,
                  scale=self.scale,
                  D=configs.D,
                  C=configs.C,
                  G=configs.G,
                  G0=configs.G0,
                  kernel_size=configs.kernel_size)

        rdn.build_model()
        tf.global_variables_initializer().run(session=self.sess)

        rdn.load(checkpoint_dir=configs.checkpoint_dir, restore=True)

        self.model = rdn

    def set_manual_seed(self):
        self.set_manual_seed_tf()

    def prepare_input(self, img_np):
        return self.prepare_input_tf(img_np)

    def do_inference(self, img_tensor):
        return self.convert_tensor_to_np_tf(self.model.eval(input_tensor=img_tensor))



if __name__ == '__main__':
    print('')