import argparse

from test_script import generate_model_tester

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--num-visualize', type=int, required=False, default=3, help='Number of Visual Comparison')
    parser.add_argument('--scale', type=int, required=False, default=4, help='Super Resolution Scale (default:4)')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    test_data_list = ['Set5', 'Set14']
    model_type_list = ['DRLN', 'SAN', 'RDN']

    for model_type in model_type_list:
        model_tester = generate_model_tester(model_type, args.scale)
        for test_data in test_data_list:
            print('[*] Start test in {} using {}'.format(test_data, model_type))
            model_tester.test(test_data_type=test_data, n_visualize=args.num_visualize, scale=args.scale)
            print('\n')