# sr_testscript

# Requirements
- pip install tensorflow-gpu==1.14
- conda install pytorch==1.5.0 torchvision==0.6.0 -c pytorch
- conda install -c anaconda scikit-learn
- conda install -c anaconda scikit-image
- conda install -c conda-forge opencv
- conda install -c conda-forge tqdm

# test 
python batch_test.py