import argparse
import sys

sys.path.append('./code')

from utils import *  # ./code/utils.py
from model.RDN.predict import RDN_ModelTester
from model.SAN.predict import SAN_ModelTester
from model.DRLN.predict import DRLN_ModelTester

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, required=True,
                        help='select model for SR 1. RDN, 2. SAN')
    parser.add_argument('--testData', type=str, required=True, hlep='select test img dataset')
    parser.add_argument('--num-visualize', type=int, required=False, default=3, help='Number of Visual Comparision')

    args = parser.parse_args()
    return args

def generate_model_tester(model_type, scale):
    if model_type == 'RDN':
        return RDN_ModelTester(scale)

    elif model_type == 'SAN':
        return SAN_ModelTester(scale)

    elif model_type == 'DRLN':
        return DRLN_ModelTester(scale)