from timeit import default_timer as timer
import abc, six, os
import cv2
import torch
import numpy as np
from tqdm import tqdm

from utils import get_psnr, get_ssim
import pdb

def get_test_dir(test_data_type):
    # Prepare test dataset path
    if test_data_type == 'Set5':
        test_dir = 'data/LR/Set5/x4'
    elif test_data_type == 'Set14':
        test_dir = 'data/LR/Set14/x4'
    else:
        print('testData should be one of [Set5, Set14]')
        raise ValueError
    return test_dir

def collect_img_file_paths(root_dir):
    img_path_list = []
    for (root, dirs, files) in os.walk(root_dir):
        img_path_list += [root + '/' + im_name for im_name in files
                          if im_name.split('.')[-1].lower() in ('jpg', 'png')]

    img_path_list.sort()

    return img_path_list

@six.add_metaclass(abc.ABCMeta)
class ModelTesterBase():
    @abc.abstractclassmethod
    def do_inference(self, img_tensor):
        pass

    @abc.abstractclassmethod
    def set_manual_seed(self):
        pass

    @abc.abstractclassmethod
    def prepare_input(self, img_np):
        pass

    def set_manual_seed_pt(self):
        if torch.cuda.is_available():
            torch.cuda.manual_seed(1234)
        else:
            torch.manual_seed(1234)
    def set_manual_seed_tf(self):
        return

    @staticmethod
    def prepare_input_pt(img_np):
        img_tensor = (np.asarray(img_np))
        # img_tensor = (np.asarray(img_np) / 255.0)

        img_tensor = torch.from_numpy(img_tensor).float()
        img_tensor = img_tensor.permute(2, 0, 1).unsqueeze(0)
        if torch.cuda.is_available():
            img_tensor = img_tensor.cuda()

        return img_tensor
    @staticmethod
    def prepare_input_tf(img_np):
        img_np = (np.asarray(img_np) / 255.0)
        img_np = np.expand_dims(img_np, axis=0)
        return img_np

    @staticmethod
    def convert_tensor_to_np_pt(img_tensor):
        img_np = img_tensor.cpu().squeeze().permute(1,2,0).numpy()# * 255.0
        return img_np

    @staticmethod
    def convert_tensor_to_np_tf(img_tensor):
        img_np = img_tensor.squeeze(0) * 255.0
        return img_np

    @staticmethod
    def load_image(img_path):
        loaded_img = cv2.imread(img_path)
        loaded_img = cv2.cvtColor(loaded_img, cv2.COLOR_BGR2RGB)
        return loaded_img

    @staticmethod
    def save_image(img_path, img_to_save):
        img_to_save = cv2.cvtColor(img_to_save, cv2.COLOR_RGB2BGR)
        cv2.imwrite(img_path, img_to_save)

    def test(self, test_data_type, n_visualize, scale):
        self.set_manual_seed()
        self.scale = scale

        # Save directory for Visual Comparision and Quantatitve result
        output_root = 'script-output/x{}/{}/'.format(scale, test_data_type)
        image_output_dir = output_root + 'images/'
        f_acc_path = output_root + 'acc_{}.txt'.format(self.model_name)
        f_runtime_path = output_root + 'avg_runtime_{}.txt'.format(self.model_name)
        os.makedirs(image_output_dir, exist_ok=True)

        # for quantitative result
        avg_psnr = []
        avg_ssim = []
        avg_run_time = []

        # for number of visual comparision
        num_visualize = 0

        # get image file list
        test_dir = get_test_dir(test_data_type)
        img_path_list = collect_img_file_paths(test_dir)

        """start evaluation! """
        for i, img_path in enumerate(tqdm(img_path_list)):
            # load image
            input_img_np = self.load_image(img_path)
            input_tensor = self.prepare_input(input_img_np)

            # Do test
            start = timer()
            output_img_np = self.do_inference(input_tensor)
            end = timer()
            run_time = end - start
            avg_run_time.append(run_time)

            output_img_np = output_img_np.clip(0, 255)
            """For Visual Comparision"""
            if num_visualize < n_visualize:
                input_img_save_path = image_output_dir + 'input_{}.png'.format(num_visualize)
                if not os.path.isfile(input_img_save_path):
                    self.save_image(image_output_dir + 'input_{}.png'.format(num_visualize), input_img_np)
                self.save_image(image_output_dir + 'output_{}_{}.png'.format(num_visualize, self.model_name),
                                output_img_np)

            num_visualize += 1

            """For Quantitative Result"""
            ref_root_dir = 'data/HR/{}/x{}'.format(test_data_type, scale)
            test_img_name = img_path.split('/')[-1]
            ref_img = os.path.join(ref_root_dir + '/' + test_img_name)

            try:
                ref_img = self.load_image(ref_img)

            except:
                ref_img = os.path.join(ref_root_dir + '/' + test_img_name.split('.')[0]+'.png')
                ref_img = self.load_image(ref_img)

            avg_psnr.append(get_psnr(output_img_np, ref_img, scale=self.scale))
            avg_ssim.append(get_ssim(output_img_np, ref_img))

        with open(f_acc_path, 'w') as f_acc:
            f_acc.write('avg PNSR: {:.5f}, avg SSIM: {:.5f}'.format(np.mean(avg_psnr),
                                                                    np.mean(avg_ssim)))

        with open(f_runtime_path, 'w') as f_runtime:
            f_runtime.write('avg runtime: {} sec'.format(np.mean(avg_run_time)))
