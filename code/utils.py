import numpy as np
import math
from skimage.metrics import structural_similarity as ssim
import pdb

def rgb2ycbcr(img):
    y = 16 + (65.481 * img[:, :, 0]) + (128.553 * img[:, :, 1]) + (24.966 * img[:, :, 2])
    return y / 255

def get_psnr(target, ref, scale):
    # target_data = np.array(target, dtype=np.float32)
    # ref_data = np.array(ref, dtype=np.float32)

    target_y = np.array(target, dtype=np.float32)
    ref_y = np.array(ref, dtype=np.float32)

    target_y = rgb2ycbcr(target_y)
    ref_y = rgb2ycbcr(ref_y)

    if not ref_y.shape == target_y.shape:
        width, height, channel = ref_y.shape
        target_y = np.resize(target_y, (width, height, channel))

    diff = ref_y - target_y

    shave = scale
    diff = diff[shave:-shave, shave:-shave]

    mse = np.mean((diff / 255) ** 2)
    if mse == 0:
        return 100

    return -10 * math.log10(mse)


def get_ssim(result, reference):
    result = np.array(result, dtype=np.float32)
    reference = np.array(reference, dtype=np.float32)

    # if not result.shape == reference.shape:
    #     width, height, channel = reference.shape
    #     result = np.resize(result, (width, height, channel))

    result = rgb2ycbcr(result)
    reference = rgb2ycbcr(reference)

    print(ssim(result, reference, data_range=255, multichannel=True))
    return ssim(result, reference, data_range=255, multichannel=True)


